import numpy as np
import chess
from copy import deepcopy
from keras.models import load_model
from chess_parse import bb2array
from preTrain import convert_To_773, deconvert_to68
from pygame import *
import pygame
from Engins.DeepChess773_CE import DeeepChess773

WIN_WIDTH = 32 * 8
WIN_HEIGHT = 32 * 8
HALF_WIDTH = int(WIN_WIDTH / 2)
HALF_HEIGHT = int(WIN_HEIGHT / 2)
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
DEPTH = 32
FLAGS = 0
CAMERA_SLACK = 30
INFINITY = 100000000

background = pygame.image.load("D:\\licenta\\ChessAi\\mnt\\resurse\\chessbg.png")
spritesheet = pygame.image.load("D:\\licenta\\ChessAi\\mnt\\resurse\\chesspieces.png")
background = pygame.transform.scale(background, (32 * 8, 32 * 8))

character = Surface((39, 42), pygame.SRCALPHA)
character.blit(spritesheet, (-3, 0))
character = pygame.transform.scale(character, (39 * 3, 42 * 3))
white_king = character

character = Surface((39, 42), pygame.SRCALPHA)
character.blit(spritesheet, (-3, -45))
character = pygame.transform.scale(character, (39 * 3, 42 * 3))
black_king = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-48, -49))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
black_queen = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-48, -3))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
white_queen = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-93, -3))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
white_bsp = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-138, -3))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
white_knight = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-182, -3))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
white_rock = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-183, -49))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
black_rock = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-138, -49))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
black_knight = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-93, -49))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
black_bsp = character

character = Surface((40, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-226, -3))
character = pygame.transform.scale(character, (40 * 3, 38 * 3))
white_pawn = character

character = Surface((40, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-226, -48))
character = pygame.transform.scale(character, (40 * 3, 38 * 3))
black_pawn = character

look_ahead = {20: 7, 19: 7, 18: 7, 17: 8, 16: 8, 15: 9, 14: 10, 13: 11, 12: 12, 11: 13, 10: 15, 9: 20, 8: 23, 7: 25,
              6: 30, 5: 40, 4: 40, 3: 40, 2: 40, 1: 40}


class Game:
    def __init__(self):
        self.board = chess.Board()
        # self.comparer = load_model('network\\DeepChess_NN_D_binary_crossentropy_100.h5')

        # graphics
        pygame.init()
        self.screen = pygame.display.set_mode(DISPLAY, FLAGS, DEPTH)
        pygame.display.set_caption(str("DeepChess"))
        self.timer = pygame.time.Clock()

        self.bg = Surface((WIN_WIDTH, WIN_HEIGHT)).convert()
        self.bg.convert()
        self.bg.fill(Color("#00455b"))

        self.camera = Camera(complex_camera, 32 * 8, 32 * 8)

        self.screen.blit(background, (0, 0))

        pygame.display.update()
        self.engine_white = DeeepChess773()
        self.engine_black = DeeepChess773()

    def display_Board(self):

        while not self.board.is_game_over():
            print()
            print(self.board)
            display_board(self.board)

            boards = []
            moves = []
            for move in self.board.legal_moves:
                moves.append(move)
                board_child = self.board.copy()
                board_child.push(move)
                boards.append(board_child)
            values = np.zeros((len(boards), 1))
            nr_pieces = len(self.board.piece_map())
            nr_look_ahead = 2
            if self.board.turn:
                for i, board in enumerate(boards):
                    values[i] = self.engine_white.get_score(self.board,board, nr_look_ahead)
                max_pos = np.argmax(values)
                self.board.push(moves[max_pos])
            else:
                for i, board in enumerate(boards):
                    values[i] = self.engine_black.get_score(self.board,board, nr_look_ahead)
                min_pos = np.argmin(values)
                self.board.push(moves[min_pos])

        print()
        print(self.board, self.board.result(), self.board.turn)


class Camera(object):
    def __init__(self, camera_func, width, height):
        self.camera_func = camera_func
        self.state = Rect(0, 0, width, height)

    def apply(self, target):
        return target.rect.move(self.state.topleft)

    def update(self, target):
        self.state = self.camera_func(self.state, target.rect)


def complex_camera(camera, target_rect):
    l, t, _, _ = target_rect
    _, _, w, h = camera
    l, t, _, _ = -l + HALF_WIDTH, -t + HALF_HEIGHT, w, h

    l = min(0, l)  # stop scrolling at the left edge
    l = max(-(camera.width - WIN_WIDTH), l)  # stop scrolling at the right edge
    t = max(-(camera.height - WIN_HEIGHT), t)  # stop scrolling at the bottom
    t = min(0, t)  # stop scrolling at the top
    return Rect(l, t, w, h)

pygame.init()
screen = pygame.display.set_mode(DISPLAY, FLAGS, DEPTH)
display.set_caption(str("DeepChess"))
timer = pygame.time.Clock()

bg = Surface((WIN_WIDTH, WIN_HEIGHT)).convert()
bg.convert()
bg.fill(Color("#00455b"))


camera = Camera(complex_camera, 32 * 8, 32 * 8)

screen.blit(background, (0, 0))

pygame.display.update()

def display_board(board):
        dict = board.piece_map()
        screen.blit(background, (0, 0))
        for key in dict:
            # print(dict[key].piece_type)
            col = int(key % 8)
            row = int(key / 8)
            # x[row * 8 + col] = int(dict[key].color) * 7 + dict[key].piece_type
            if dict[key].piece_type == 1:
                p = Pawn(col * 32, row * 32, color=dict[key].color)
                screen.blit(p.image, camera.apply(p))
            elif dict[key].piece_type == 2:
                p = Knight(col * 32, row * 32, color=dict[key].color)
                screen.blit(p.image, camera.apply(p))
            elif dict[key].piece_type == 3:
                p = Bsp(col * 32, row * 32, color=dict[key].color)
                screen.blit(p.image, camera.apply(p))
            elif dict[key].piece_type == 4:
                p = Rook(col * 32, row * 32, color=dict[key].color)
                screen.blit(p.image, camera.apply(p))
            elif dict[key].piece_type == 5:
                p = Queen(col * 32, row * 32, color=dict[key].color)
                screen.blit(p.image, camera.apply(p))
            elif dict[key].piece_type == 6:
                p = King(col * 32, row * 32, color=dict[key].color)
                screen.blit(p.image, camera.apply(p))
        pygame.display.update()


class Piece(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)


class King(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_king
        else:
            self.image = white_king
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Queen(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_queen
        else:
            self.image = white_queen
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Bsp(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_bsp
        else:
            self.image = white_bsp
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Rook(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_rock
        else:
            self.image = white_rock
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Knight(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_knight
        else:
            self.image = white_knight
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Pawn(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_pawn
        else:
            self.image = white_pawn
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


if __name__ == "__main__":
    # init_Display()
    game = Game()
    game.display_Board()


def restart():
    pygame.init()
    screen = pygame.display.set_mode(DISPLAY, FLAGS, DEPTH)
    display.set_caption(str("DeepChess"))
    timer = pygame.time.Clock()

    bg = Surface((WIN_WIDTH, WIN_HEIGHT)).convert()
    bg.convert()
    bg.fill(Color("#00455b"))

    camera = Camera(complex_camera, 32 * 8, 32 * 8)

    screen.blit(background, (0, 0))

    pygame.display.update()