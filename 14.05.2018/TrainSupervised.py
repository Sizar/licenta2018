import keras
from keras.models import Model, load_model
from keras.layers.core import Dense, Dropout, Activation
from keras.layers import Input, Layer, regularizers
from preTrain import getXWon, getXLost,getXLost1,getXWon1
from keras import optimizers, models
import numpy as np
import os
import random


def loadModel(path):
    yaml_file = open(path, "r")
    model_yaml = yaml_file.read()
    yaml_file.close()
    return models.model_from_yaml(model_yaml)

class DeepChess:
    def __init__(self):
        # get the model that encode the states
        encoder = load_model("pretrain_NN_E_ADAM12.h5")
        encoder.load_weights("pretrain_NN_E_ADAM12_weights.h5")

        # createing the firsts layers
        input1 = Input(shape=(773,))  # the input
        encoded1 = Dense(600, activation='relu', use_bias=True,
                         activity_regularizer=regularizers.l2(10e-5))(input1)
        encoded1 = Dense(400, activation='relu', use_bias=True,
                         activity_regularizer=regularizers.l2(10e-5))(encoded1)
        encoded1 = Dense(200, activation='relu', use_bias=True,
                         activity_regularizer=regularizers.l2(10e-5))(encoded1)
        encoded1 = Dense(100, activation='relu', use_bias=True,
                         activity_regularizer=regularizers.l2(10e-5))(encoded1)

        input2 = Input(shape=(773,))  # the input
        encoded2 = Dense(600, activation='relu', use_bias=True,
                         activity_regularizer=regularizers.l2(10e-5))(input2)
        encoded2 = Dense(400, activation='relu', use_bias=True,
                         activity_regularizer=regularizers.l2(10e-5))(encoded2)
        encoded2 = Dense(200, activation='relu', use_bias=True,
                         activity_regularizer=regularizers.l2(10e-5))(encoded2)
        encoded2 = Dense(100, activation='relu', use_bias=True,
                         activity_regularizer=regularizers.l2(10e-5))(encoded2)

        # set the weights
        x = keras.layers.concatenate([encoded1, encoded2])

        # create the top layers for the actualy comparing the to states
        hidden_layer = Dense(400, activation='relu', use_bias=True)(x)
        hidden_layer = Dense(200, activation='relu', use_bias=True)(hidden_layer)
        hidden_layer = Dense(100, activation='relu', use_bias=True)(hidden_layer)
        hidden_layer = Dense(2, activation='softmax')(hidden_layer)
        self.deepChees = Model(inputs=[input1, input2], outputs=hidden_layer)

        # now lets fix the layers form the start of our network
        j = 1
        for i in range(2, 10, 2):
            # print(i)
            self.deepChees.layers[i].set_weights(encoder.layers[j].get_weights())
            self.deepChees.layers[i + 1].set_weights(encoder.layers[j].get_weights())
            j += 1
        # configure the optimizer and loss, here I used adadelta and binart_crssentropy, feel free to try to explore
        # self.deepChees.compile(optimizer='adadelta', loss='binary_crossentropy')

        print("################# Init done #################")

    def train(self):
        #self.deepChees = load_model("networks/DeepChess_new_Try_285.h5")
        print("################# Starting the training #################")
        # self.deepChees =load_model("D:\\licenta\\ChessAi\\networks\\DeepChess_NN_D_binary_crossentropy_100.h5")
        # self.deepChees = loadModel("networks/W_DeepChess_model.yaml")
        # self.deepChees.load_weights("networks/W_DeepChess_weights.h5")

        x_Lose = getXLost()
        x_Won = getXWon()

        # it's better to compute 1 single time
        n_Won = len(x_Won)
        n_Lose = len(x_Lose)

        # print(np.concatenate((x_Lose[0], x_Won[0])).shape)

        # train data
        x_train1 = np.zeros(shape=(300000, 773),
                            dtype="float32")  # 10000000 of inputs ,2 * 100 the size of the encode.predict(...)
        x_train2 = np.zeros(shape=(300000, 773),
                            dtype="float32")  # 10000000 of inputs ,2 * 100 the size of the encode.predict(...)
        y_train = np.zeros((300000, 2))  # 1000000 of outputs

        # validating data

        x_validate1 = np.zeros(shape=(10000, 773),
                               dtype="float32")  # 1000000 of inputs ,2 * 100 the size of the encode.predict(...)
        x_validate2 = np.zeros(shape=(10000, 773),
                               dtype="float32")  # 1000000 of inputs ,2 * 100 the size of the encode.predict(...)
        y_validate = np.zeros((10000, 2))  # 1000000 of outputs

        i = 0
        while i < 10000:

            if random.randint(0, 1) == 1:
                indexWon = random.randint(0, n_Won - 1)
                indexLose = random.randint(0, n_Lose - 1)
                x_validate1[i] = np.copy(x_Lose[indexLose])
                x_validate2[i] = np.copy(x_Won[indexWon])
                y_validate[i][1] = 1
            else:
                indexWon = random.randint(0, n_Won - 1)
                indexLose = random.randint(0, n_Lose - 1)
                x_validate1[i] = np.copy(x_Won[indexWon])
                x_validate2[i] = np.copy(x_Lose[indexLose])
                y_validate[i][0] = 1
                i += 1
        j = 0
        while j < 200:

            # gating the data for training
            for i in range(300000):
                if random.randint(0, 1) == 1:
                    indexWon = random.randint(0, n_Won - 1)
                    indexLose = random.randint(0, n_Lose - 1)
                    x_train1[i] = np.copy(x_Lose[indexLose])
                    x_train2[i] = np.copy(x_Won[indexWon])
                    y_train[i][1] = 1
                else:
                    indexWon = random.randint(0, n_Won - 1)
                    indexLose = random.randint(0, n_Lose - 1)
                    x_train1[i] = np.copy(x_Won[indexWon])
                    x_train2[i] = np.copy(x_Lose[indexLose])
                    y_train[i][0] = 1

            # x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
            # x_validate = x_validate.reshape((len(x_validate), np.prod(x_validate.shape[1:])))
            # print("ok")
            print(j)
            self.deepChees.compile(
                optimizer=optimizers.SGD(lr=0.01* 0.99**j,momentum=0.0,nesterov=False),
                loss='categorical_crossentropy',
                metrics=['mae', 'acc'],
            )

            self.deepChees.fit([x_train1, x_train2], y_train,
                               epochs=1,
                               batch_size=256,
                               shuffle=True,
                               validation_data=([x_validate1,x_validate2],y_validate)
                               )

            if j % 5 == 0:
                modelToSave = self.deepChees.to_yaml()
                with open("networks/W_DeepChess_model.yaml", "w") as yaml_file:
                    yaml_file.write(modelToSave)

                self.deepChees.save_weights("networks/W_DeepChess_weights.h5")
                # dpCopy = loadModel("networks/W_DeepChess_model.yaml")
                # dpCopy.load_weights("networks/W_DeepChess_weights.h5")
                # for i,layer in enumerate(self.deepChees.layers):
                #     for j,weights in enumerate(layer.get_weights()):
                #         print("w = ",(weights-dpCopy.layers[i].get_weights()[j]).any())

                # self.deepChees = loadModel("networks/W_DeepChess_model.yaml")
                #
                # self.deepChees.load_weights("networks/W_DeepChess_weights.h5")
                # x_won_new = getXWon()
                # for i, x in enumerate(x_Won):
                #     print((x-x_won_new[i]).any())
                x_Won = getXWon()
                x_Lose = getXLost()
            j += 1
        print("################# Training done! #################")

    def evaluate(self):
        self.deepChees = loadModel("networks/W_DeepChess_model.yaml")
        self.deepChees.load_weights("networks/W_DeepChess_weights.h5")
        self.deepChees.compile(
            optimizer=optimizers.SGD(lr=0.01, momentum=0.0, nesterov=False),
            loss='categorical_crossentropy',
            metrics=['mae', 'acc'],
        )
        x_Lose = getXLost()
        x_Won = getXWon()

        # it's better to compute 1 single time
        n_Won = len(x_Won)
        n_Lose = len(x_Lose)

        # print(np.concatenate((x_Lose[0], x_Won[0])).shape)

        # train data
        x_train1 = np.zeros(shape=(200000, 773),
                            dtype="float32")  # 10000000 of inputs ,2 * 100 the size of the encode.predict(...)
        x_train2 = np.zeros(shape=(200000, 773),
                            dtype="float32")  # 10000000 of inputs ,2 * 100 the size of the encode.predict(...)
        y_train = np.zeros((200000, 2))  # 1000000 of outputs



        j = 0
        while j < 10:

            # gating the data for training
            for i in range(200000):
                if random.randint(0, 1) == 1:
                    indexWon = random.randint(0, n_Won - 1)
                    indexLose = random.randint(0, n_Lose - 1)
                    x_train1[i] = np.copy(x_Lose[indexLose])
                    x_train2[i] = np.copy(x_Won[indexWon])
                    y_train[i][1] = 1
                else:
                    indexWon = random.randint(0, n_Won - 1)
                    indexLose = random.randint(0, n_Lose - 1)
                    x_train1[i] = np.copy(x_Won[indexWon])
                    x_train2[i] = np.copy(x_Lose[indexLose])
                    y_train[i][0] = 1

            # x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
            # x_validate = x_validate.reshape((len(x_validate), np.prod(x_validate.shape[1:])))
            # print("ok")

            _, _, acc= self.deepChees.evaluate([x_train1, x_train2], y_train,verbose=0)

            print(j," acc = ", acc)
            j += 1
        print("################# Training done! #################")


if __name__ == "__main__":
    dc = DeepChess()
    #dc.train()
    dc.evaluate()
    # dc.deepChees.predict()
