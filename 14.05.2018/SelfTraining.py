from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import Nadam
from keras.models import load_model
from keras import optimizers, Model, Input, regularizers
from collections import deque
import random
import numpy as np
import tensorflow as tf
import chess
from preTrain import convert_To_773
from chess_parse import bb2array
import chessInterface as CI
import os
from chess_parse import read_games
from copy import deepcopy

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
from keras import backend as K

K.set_session(sess)

CHECKMATE = 1

model = load_model("networks/DeepChess_NN_ADAM_25.h5")

def getNewModel():
    encoder = load_model("pretrain_NN_E_Adam.h5")

    # createing the firsts layers
    input1 = Input(shape=(773,))  # the input
    encoded1 = Dense(600, activation='relu', use_bias=True,
                     activity_regularizer=regularizers.l2(10e-5))(input1)
    encoded1 = Dense(400, activation='relu', use_bias=True,
                     activity_regularizer=regularizers.l2(10e-5))(encoded1)
    encoded1 = Dense(200, activation='relu', use_bias=True,
                     activity_regularizer=regularizers.l2(10e-5))(encoded1)
    encoded1 = Dense(100, activation='relu', use_bias=True,
                     activity_regularizer=regularizers.l2(10e-5))(encoded1)

    input2 = Input(shape=(773,))  # the input
    encoded2 = Dense(600, activation='relu', use_bias=True,
                     activity_regularizer=regularizers.l2(10e-5))(input2)
    encoded2 = Dense(400, activation='relu', use_bias=True,
                     activity_regularizer=regularizers.l2(10e-5))(encoded2)
    encoded2 = Dense(200, activation='relu', use_bias=True,
                     activity_regularizer=regularizers.l2(10e-5))(encoded2)
    encoded2 = Dense(100, activation='relu', use_bias=True,
                     activity_regularizer=regularizers.l2(10e-5))(encoded2)

    # set the weights
    x = tf.keras.layers.concatenate([encoded1, encoded2])

    # create the top layers for the actualy comparing the to states
    hidden_layer = Dense(400, activation='relu', use_bias=True)(x)
    hidden_layer = Dense(200, activation='relu', use_bias=True)(hidden_layer)
    hidden_layer = Dense(100, activation='relu', use_bias=True)(hidden_layer)
    hidden_layer = Dense(2, activation='softmax')(hidden_layer)
    deepChess = Model(inputs=[input1, input2], outputs=hidden_layer)

    # now lets fix the layers form the start of our network
    j = 1
    for i in range(2, 10, 2):
        # print(i)
        deepChess.layers[i].set_weights(encoder.layers[j].get_weights())
        deepChess.layers[i + 1].set_weights(encoder.layers[j].get_weights())
        j += 1
    return deepChess


def parse_game(g):
    r = g.headers['Result']
    # verify if the game is won by black/white
    if r == '1/2-1/2':  # draw
        return None
    elif r == '1-0':
        r = 1
    else:
        r = -1
    # Generate all boards
    gn = g.end()
    if not gn.board().is_game_over():
        return None

    xs = []
    gns = []
    n = 0
    # make sure that the final move is recorded
    xs.append((gn, gn.parent, r))
    while gn:
        gns.append((gn, gn.parent, r))
        # print(gn.board().turn)
        gn = gn.parent
        n += 1

    # print(len(gns))
    # if len(gns) < 10:
    # print(g.end())

    if (len(gns) > 6):
        for i in range(6):
            gns.pop()
    else:
        # We don't need the first 8 mouves / the openings / it will Q-learn them later
        return None
    n -= 6
    for i in range(min(5,int(n / 2))):
        gn = random.choice(gns)
        xs.append(gn)
    return xs

def fill_buffer():
    d = 'D:\\licenta\\ChessAi\\mnt\\games'
    buffer = deque()
    while len(buffer) < 10000:
        list_files = os.listdir(d)
        random.shuffle(list_files)
        for fn_in in list_files:
            if not fn_in.endswith('.pgn'):
                continue
            print(len(buffer))
            try:
                fn_in = os.path.join(d, fn_in)

                for game in read_games(fn_in):
                    xs = parse_game(game)
                    if xs is None:
                        continue
                    for board_child, board_parent, rew in xs:
                        new_board = board_child.board().copy()
                        buffer.append((board_parent.board(), board_child.board().pop(), rew, new_board))
                    print(len(buffer))
                    if len(buffer) > 10000:
                        break
                if len(buffer) > 10000:
                    break
            except:
                pass

    return buffer


def train_The_Network(model):
    model.compile(loss='categorical_crossentropy',
                  optimizer=Nadam(lr=0.002, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004))
    observetime = 5000
    epsilon = 0.9
    epsilon_final = 0.1
    epsilon_initial = 0.5
    explore = 1000
    batchSize = 64
    buffer = 100000
    replay = fill_buffer()
    replay_game = deque()

    board = None
    games_Won = 0
    game_Drow = 0
    nr_epoch = 0
    while True:
        nr_epoch += 1
        moves_teaken = 0
        board = chess.Board()
        while not board.is_game_over():
            CI.restart()
            CI.display_board(board)
            moves_teaken += 1
            action = None
            moves = []
            child_boards_converted = []
            for move in board.legal_moves:
                moves.append(move)
                child_board = board.copy()
                child_board.push(move)
                child_boards_converted.append(convert_for_predict(child_board))

            board_converted_4_predict = convert_for_predict(board)
            randnr = random.random()
            if (randnr < epsilon):  # choose random action
                index_max_Qvalue = np.random.randint(0, len(moves))
            else:  # choose best action from Q(s,a) values
                max_value = -10000
                for i, child in enumerate(child_boards_converted):
                    prediction = model.predict(x=[board_converted_4_predict, child])
                    dif = prediction[0][1] - prediction[0][0]
                    if max_value < dif:
                        max_value = dif
                        index_max_Qvalue = i

            # print(len(moves))
            action = moves[index_max_Qvalue]
            # if not board.turn:  # skip the black moves
            #     board.push(action)
            #     continue

            input_state = board.copy()
            board.push(action)
            reward = getReward(board)
            replay.append((input_state, action, reward, board))
            if replay.__len__() < buffer:
                replay.append((input_state.copy(), action, reward, board.copy()))
            else:
                replay.popleft()
                replay.append((input_state.copy(), action, reward, board.copy()))
            # take the best Q move for black
            if reward == 0:
                moves = []
                child_boards_converted = []
                for move in board.legal_moves:
                    moves.append(move)
                    child_board = board.copy()
                    child_board.push(move)
                    child_boards_converted.append(convert_for_predict(child_board))

                max_value = -10000
                randnr = random.random()
                if (randnr < epsilon):  # choose random action
                    index_max_Qvalue = np.random.randint(0, len(moves))
                else:
                    for i, child in enumerate(child_boards_converted):
                        prediction = model.predict(x=[board_converted_4_predict, child])
                        dif = prediction[0][0] - prediction[0][1]
                        if max_value < dif:
                            max_value = dif
                            index_max_Qvalue = i
                action = moves[index_max_Qvalue]
                input_state = board.copy()
                board.push(action)
                reward = getReward(board)
                if replay.__len__() < buffer:
                    replay.append((input_state.copy(), action, reward, board.copy()))
                else:
                    replay.popleft()
                    replay.append((input_state, action, reward, board))
            # Observe reward
            # reward = gm.getReward(new_state,state)

            if nr_epoch % 1000 == 0 and len(replay) >= observetime:
                # print(gm.dispGrid(state))
                # print(action)
                model.save("networks\\SelfTrainingModel.h5")
                model.save_weights('my_model_weights.h5')
                model_json = model.to_json()
                with open("model.json", "w") as json_file:
                    json_file.write(model_json)

                # f.write(gm.dispGrid(state)+"\n")
                # f.write(action+"\n")

            # randomly sample our experience replay memory
            if len(replay) >= observetime:
                minibatch = random.sample(replay, batchSize)
                X_train = []
                y_train = []
                for memory in minibatch:
                    old_state, action_m, reward_m, new_state_m = memory
                    old_b_4_predict = convert_for_predict(old_state)
                    old_state_turn = old_state.turn
                    to_compare_with = convert_for_predict(new_state_m)

                    y = np.zeros((1, 2))

                    if reward_m == 0:  # miscarea nu a dus la remiza
                        # print(reward_m)
                        old_qval = model.predict(x=[old_b_4_predict, to_compare_with])

                        child_boards_converted = []
                        # terminal_child = False
                        for move in new_state_m.legal_moves:
                            child_board = new_state_m.copy()
                            child_board.push(move)
                            child_boards_converted.append(convert_for_predict(child_board))

                        if not old_state_turn:
                            max_q = -10000
                            index_state = 0
                            for q, child_board in enumerate(child_boards_converted):
                                prediction = model.predict(x=[to_compare_with, child_board])
                                dif = prediction[0][1] - prediction[0][0]
                                if max_q < dif:
                                    index_state = q
                                    max_q = dif
                            y = model.predict(x=[old_b_4_predict, child_boards_converted[index_state]])

                        else:
                            min_q = 10000
                            index_state = 0
                            for q, child_board in enumerate(child_boards_converted):
                                prediction = model.predict(x=[to_compare_with, child_board])
                                dif = prediction[0][1] - prediction[0][0]
                                if min_q > dif:
                                    index_state = q
                                    min_q = dif

                            y = model.predict(x=[old_b_4_predict, child_boards_converted[index_state]])
                        if (y[0] - old_qval[0]).any():
                            print(y, " ", old_qval)
                        print("newq_val:", y, "old:", old_qval, "turn", old_state_turn, old_state == new_state_m)
                    elif reward_m == 1:  # miscarea a dus la sah mat
                        y[0][1] = 1
                        y[0][0] = 0
                    else:  # miscarea a dus la o pozitie din care s-a luat sah mat
                        y[0][1] = 0
                        y[0][0] = 1
                    #print(reward_m, y)
                    X_train.append([old_b_4_predict, to_compare_with])
                    y_train.append(y)

                # X_train = np.array(X_train)
                # y_train = np.array(y_train)

                # loss=model.train_on_batch(X_train,y_train)
                n = len(X_train)
                for j in range(n):
                    model.fit(X_train[j], y_train[j], batch_size=batchSize, epochs=1, verbose=0)
            # lossmean+=loss

        if epsilon > epsilon_final and len(replay) > observetime:  # decrement epsilon over time
            epsilon -= (epsilon_initial - epsilon_final) / explore

        # print(board)
        if board.is_checkmate():
            games_Won += 1
            if board.turn:
                print("White lost")
            else:
                print("Black lost")
        else:
            game_Drow += 1
        print("Games won:", games_Won, " Games drowned: ", game_Drow, "Epsilon:", epsilon, "Game:", nr_epoch)


def convert_for_predict(board):
    i = convert_To_773(bb2array(board))
    x_pred = np.zeros(shape=(1, 773),
                      dtype="float32")
    x_pred[0] = i
    return x_pred


def getReward(board):
    if board.is_checkmate():
        if board.turn:
            return 1
        else:
            return -1
    if board.is_game_over():
        return -0.5
    return 0


if __name__ == "__main__":
    train_The_Network(model)
    # board  = chess.Board()
    # board.push_san("e4")
    # print(board.pop())
