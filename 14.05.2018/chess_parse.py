import chess, chess.pgn
import numpy
import sys
import os
import multiprocessing
import itertools
import random
import h5py


def read_games(fn):
    f = open(fn)

    while True:
        try:
            g = chess.pgn.read_game(f)
        except KeyboardInterrupt:
            raise
        except:
            continue

        if not g:
            break

        yield g


def bb2array(b):
    x = numpy.zeros(68, dtype=numpy.int8)

    dict = b.piece_map()
    for key in dict:
        # print(dict[key].piece_type)
        col = int(key % 8)
        row = int(key / 8)
        x[row * 8 + col] = int(dict[key].color) * 7 + dict[key].piece_type

    x[64] = b.has_kingside_castling_rights(color=True)
    x[65] = b.has_queenside_castling_rights(color=True)
    x[66] = b.has_kingside_castling_rights(color=False)
    x[67] = b.has_queenside_castling_rights(color=False)
    # print(x[0:64].reshape((8, 8)))
    return x


def parse_game(g):
    r = g.headers['Result']
    # verify if the game is won by black/white
    if r != '1-0': #black wins
        return None

    # Generate all boards
    gn = g.end()
    if not gn.board().is_game_over():
        return None

    gns = []
    n = 0
    while gn:
        gns.append((gn))
        # print(gn.board().turn)
        gn = gn.parent
        n += 1

    # print(len(gns))
    # if len(gns) < 10:
    # print(g.end())

    if (len(gns) > 6):
        for i in range(6):
            gns.pop()
    else:
        # We don't need the first 8 mouves / the openings / it will Q-learn them later
        return None
    xs = []
    n -= 6
    for i in range(int(n / 2)):
        gn = random.choice(gns)

        b = gn.board()
        x = bb2array(b)

        xs.append(x)
    return xs


def read_all_games(fn_in, fn_out):
    g = h5py.File(fn_out, 'w')  # 68 *12 (1,2,3,4,5,6,7,8,9,10,11,12
    X = g.create_dataset("x", (0, 68), dtype='b', maxshape=(None, 68), chunks=True)
    size = 0
    line = 0
    for game in read_games(fn_in):
        game = parse_game(game)
        if game is None:
            continue
        xs = game
        for x in xs:
            if line + 1 >= size:
                g.flush()
                size = 2 * size + 1
                # print('resizing to', size)
                X.resize(size=size, axis=0)

            X[line] = x
            line += 1

    X.resize(size=line, axis=0)
    # print('resizing to asdas', line)
    print("file", fn_in, " had: ", line)
    g.close()


def read_all_games_2(a):
    read_all_games(*a)


def parse_dir():
    files = []
    i = 0
    d = 'D:\licenta\ChessAi\mnt\games'

    for fn_in in os.listdir(d):
        if not fn_in.endswith('.pgn'):
            continue
        fn_in = os.path.join(d, fn_in)
        fn_out = os.path.join("D:\\licenta\\ChessAi\\mnt\\games\\WhiteWon", str(i) + '.hdf5')
        # f = open(fn_out,"wb")
        files.append((fn_in, fn_out))
        i += 1
    pool = multiprocessing.Pool()
    pool.map(read_all_games_2, files)


if __name__ == '__main__':
    parse_dir()