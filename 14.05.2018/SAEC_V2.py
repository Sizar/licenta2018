from keras.layers import Input, Dense, regularizers
from keras.models import Model, load_model
from keras import optimizers
import h5py
import numpy as np
import pickle as pk
import os
from copy import deepcopy
import random


def convert_To_773(vector_68):
    array_773 = np.zeros(shape=(773,))
    array_773[-4:] = vector_68[-4:]
    for i in range(64):
        if vector_68[i] == 1:
            array_773[i] = 1
        elif vector_68[i] == 2:
            array_773[64 + i] = 1
        elif vector_68[i] == 3:
            array_773[64 * 2 + i] = 1
        elif vector_68[i] == 4:
            array_773[64 * 3 + i] = 1
        elif vector_68[i] == 5:
            array_773[64 * 4 + i] = 1
        elif vector_68[i] == 6:
            array_773[64 * 5 + i] = 1
        elif vector_68[i] == 7:
            array_773[64 * 6 + i] = 1
        elif vector_68[i] == 8:
            array_773[64 * 7 + i] = 1
        elif vector_68[i] == 9:
            array_773[64 * 8 + i] = 1
        elif vector_68[i] == 10:
            array_773[64 * 9 + i] = 1
        elif vector_68[i] == 11:
            array_773[64 * 10 + i] = 1
        elif vector_68[i] == 12:
            array_773[64 * 11 + i] = 1

    return array_773


def deconvert_to68(array_773):
    array_773 = array_773[0]
    vector_68 = np.zeros(shape=(64,))
    for i in range(64):
        if array_773[i] == 1:
            vector_68[i] = 1
        elif array_773[64 + i] == 1:
            vector_68[i] = 2
        elif array_773[64 * 2 + i] == 1:
            vector_68[i] = 3
        elif array_773[64 * 3 + i] == 1:
            vector_68[i] = 4
        elif array_773[64 * 4 + i] == 1:
            vector_68[i] = 5
        elif array_773[64 * 5 + i] == 1:
            vector_68[i] = 6
        elif array_773[64 * 6 + i] == 1:
            vector_68[i] = 7
        elif array_773[64 * 7 + i] == 1:
            vector_68[i] = 8
        elif array_773[64 * 8 + i] == 1:
            vector_68[i] = 9
        elif array_773[64 * 9 + i] == 1:
            vector_68[i] = 10
        elif array_773[64 * 10 + i] == 1:
            vector_68[i] = 11
        elif array_773[64 * 11 + i] == 1:
            vector_68[i] = 12

    return vector_68.reshape((8, 8))


def convert_List_68_773(list, limit):
    list_modificata = []
    i = 0
    for item in list:
        list_modificata.append(convert_To_773(item))
        if i >= limit:
            break
        i += 1
    print("here", list_modificata.__len__())
    return list_modificata


def getCurentXLost():
    dir = "D:\\licenta\\ChessAi\\mnt\\games\\WhiteLose"
    g2 = h5py.File('D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_LostData2_773.hdf5',
                   'w')  # 68 *12 (1,2,3,4,5,6,7,8,9,10,11,12
    X_lose = g2.create_dataset("x", (0, 773), dtype='b', maxshape=(None, 773), chunks=True)
    training_set = np.zeros((0, 773))
    for file in os.listdir(dir):
        if file.endswith('.hdf5'):
            f = h5py.File(os.path.join(dir, file), "r")
            for item in f.keys():
                # print(item,type(f[item].value),len(f[item].value))

                if len(training_set) < 900000:
                    training_set = np.append(training_set,
                                             convert_List_68_773(f[item].value, 900000 - len(training_set)), axis=0)
                    print("aiciL", training_set.shape, len(training_set))
                else:
                    line = len(X_lose)
                    X_lose.resize(size=line + len(training_set), axis=0)
                    for x in training_set:
                        X_lose[line] = x
                    training_set = np.zeros((0, 773))
    g2.close()


def getCurentXWon():
    dir = "D:\\licenta\\ChessAi\\mnt\\games\\WhiteWon"
    g1 = h5py.File('D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_WonData2_773.hdf5',
                   'w')  # 68 *12 (1,2,3,4,5,6,7,8,9,10,11,12
    X_won = g1.create_dataset("x", (0, 773), dtype='b', maxshape=(None, 773), chunks=True)
    training_set = np.zeros((0, 773))
    for file in os.listdir(dir):
        if file.endswith('.hdf5'):
            f = h5py.File(os.path.join(dir, file), "r")
            for item in f.keys():
                # print(item,type(f[item].value),len(f[item].value))0
                if len(training_set) < 900000:
                    training_set = np.append(training_set,
                                             convert_List_68_773(f[item].value, 900000 - len(training_set)), axis=0)
                    print("aiciW", training_set.shape, len(training_set))
                else:
                    line = len(X_won)
                    X_won.resize(size=line + len(training_set), axis=0)
                    for x in training_set:
                        X_won[line] = x
                    training_set = np.zeros((0, 773))
    g1.close()


def getValidation_data():
    val_data = np.zeros((0, 773))
    f1 = "D:\licenta\\ChessAi\\mnt\\games\\whiteLose.hdf5"
    f2 = "D:\licenta\\ChessAi\\mnt\\games\\whiteWon.hdf5"
    f1 = h5py.File(f1, "r")
    f2 = h5py.File(f2, "r")
    for item in f1.keys():
        val_data = np.append(val_data, convert_List_68_773(f1[item].value, limit=10000), axis=0)
    for item in f2.keys():
        val_data = np.append(val_data, convert_List_68_773(f2[item].value, limit=10000), axis=0)
    return np.array(val_data)


def getXWon():
    print("ok")
    f = h5py.File("D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_WonData_773.hdf5", "r")
    for key in f.keys():
        if key == "x":
            return f[key].value


def getXLost():
    print("ok")
    f = h5py.File("D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_LostData_773.hdf5", "r")
    for key in f.keys():
        if key == "x":
            return f[key].value


def get_training_data():
    # getCurentXLost()
    # getCurentXWon()
    x_won = getXWon()
    x_lose = getXLost()
    return np.append(x_won, x_lose, axis=0)


class SAEC:
    def __init__(self):
        self.x_train = get_training_data()
        self.x_train = self.x_train.astype('float32')

        # this is our input placeholder
        input_img = Input(shape=(773,))
        # "encoded" is the encoded representation of the input
        encoded = Dense(600, activation='relu', activity_regularizer=regularizers.l2(10e-5))(input_img)
        encoded = Dense(400, activation='relu', activity_regularizer=regularizers.l2(10e-5))(encoded)
        encoded = Dense(200, activation='relu', activity_regularizer=regularizers.l2(10e-5))(encoded)
        encoded = Dense(100, activation='relu', activity_regularizer=regularizers.l2(10e-5))(encoded)

        decoded = Dense(200, activation='relu', activity_regularizer=regularizers.l2(10e-5))(encoded)
        decoded = Dense(400, activation='relu', activity_regularizer=regularizers.l2(10e-5))(decoded)
        decoded = Dense(600, activation='relu', activity_regularizer=regularizers.l2(10e-5))(decoded)
        decoded = Dense(773, activation='sigmoid')(decoded)

        # this model maps an input to its reconstruction
        self.autoencoder = Model(input_img, decoded)

        self.autoencoder.compile(
            optimizer=optimizers.SGD(lr=0.005, momentum=0.9, nesterov=True),
            loss='mean_squared_error',
            metrics=['mae', 'acc'])
        x_train = self.x_train.reshape((len(self.x_train), np.prod(self.x_train.shape[1:])))

        print("ok")
        self.autoencoder.fit(x_train, x_train,
                             epochs=20,
                             batch_size=256,
                             shuffle=True
                             )

        self.encoder = Model(input_img, encoded)
        encoded_input = Input(shape=(100,))
        decoder_layer = self.autoencoder.layers[-4](encoded_input)
        decoder_layer = self.autoencoder.layers[-3](decoder_layer)
        decoder_layer = self.autoencoder.layers[-2](decoder_layer)
        decoder_layer = self.autoencoder.layers[-1](decoder_layer)
        self.decoder = Model(encoded_input, decoder_layer)

s = SAEC()
