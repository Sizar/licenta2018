from keras.layers import Input, Dense, regularizers
from keras.models import Model, load_model
from keras import optimizers
from TrainUtils import loadModel
import h5py
import numpy as np
import pickle as pk
import os
from copy import deepcopy
import random
import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
from keras import backend as K

K.set_session(sess)


def convert_To_773(vector_68):
    array_773 = np.zeros(shape=(773,))
    array_773[-4:] = vector_68[-4:]
    for i in range(64):
        if vector_68[i] == 1:
            array_773[i] = 1
        elif vector_68[i] == 2:
            array_773[64 + i] = 1
        elif vector_68[i] == 3:
            array_773[64 * 2 + i] = 1
        elif vector_68[i] == 4:
            array_773[64 * 3 + i] = 1
        elif vector_68[i] == 5:
            array_773[64 * 4 + i] = 1
        elif vector_68[i] == 6:
            array_773[64 * 5 + i] = 1
        elif vector_68[i] == 7:
            array_773[64 * 6 + i] = 1
        elif vector_68[i] == 8:
            array_773[64 * 7 + i] = 1
        elif vector_68[i] == 9:
            array_773[64 * 8 + i] = 1
        elif vector_68[i] == 10:
            array_773[64 * 9 + i] = 1
        elif vector_68[i] == 11:
            array_773[64 * 10 + i] = 1
        elif vector_68[i] == 12:
            array_773[64 * 11 + i] = 1

    return array_773


def deconvert_to68(array_773):
    array_773 = array_773[0]
    vector_68 = np.zeros(shape=(64,))
    for i in range(64):
        if array_773[i] == 1:
            vector_68[i] = 1
        elif array_773[64 + i] == 1:
            vector_68[i] = 2
        elif array_773[64 * 2 + i] == 1:
            vector_68[i] = 3
        elif array_773[64 * 3 + i] == 1:
            vector_68[i] = 4
        elif array_773[64 * 4 + i] == 1:
            vector_68[i] = 5
        elif array_773[64 * 5 + i] == 1:
            vector_68[i] = 6
        elif array_773[64 * 6 + i] == 1:
            vector_68[i] = 7
        elif array_773[64 * 7 + i] == 1:
            vector_68[i] = 8
        elif array_773[64 * 8 + i] == 1:
            vector_68[i] = 9
        elif array_773[64 * 9 + i] == 1:
            vector_68[i] = 10
        elif array_773[64 * 10 + i] == 1:
            vector_68[i] = 11
        elif array_773[64 * 11 + i] == 1:
            vector_68[i] = 12

    return vector_68.reshape((8, 8))


def convert_List_68_773(list, limit):
    list_modificata = []
    i = 0
    n = len(list)
    while i < min(n, limit):
        index = np.random.randint(0, n)
        list_modificata.append(convert_To_773(list[index]))
        i += 1

    print("here", list_modificata.__len__())
    return list_modificata


def getCurentXLost():
    dir = "D:\\licenta\\ChessAi\\mnt\\games\\WhiteLose"
    g2 = h5py.File('D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_LostData2_773.hdf5',
                   'w')  # 68 *12 (1,2,3,4,5,6,7,8,9,10,11,12
    X_lose = g2.create_dataset("x", (0, 773), dtype='b', maxshape=(None, 773), chunks=True)
    training_set = np.zeros((0, 773))
    files = os.listdir(dir)
    random.shuffle(files)
    for file in files:
        if file.endswith('.hdf5'):
            f = h5py.File(os.path.join(dir, file), "r")
            for item in f.keys():
                # print(item,type(f[item].value),len(f[item].value))

                if len(training_set) < 50000:
                    training_set = np.append(training_set,
                                             convert_List_68_773(f[item].value, 50000 - len(training_set)), axis=0)
                    print("aiciL", training_set.shape, len(X_lose))
                else:
                    line = len(X_lose)
                    X_lose.resize(size=line + len(training_set), axis=0)
                    for x in training_set:
                        X_lose[line] = x
                    break
            if len(X_lose) >= 50000:
                break
    g2.close()


def getCurentXWon():
    dir = "D:\\licenta\\ChessAi\\mnt\\games\\WhiteWon"
    g1 = h5py.File('D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_WonData2_773.hdf5',
                   'w')  # 68 *12 (1,2,3,4,5,6,7,8,9,10,11,12
    X_won = g1.create_dataset("x", (0, 773), dtype='b', maxshape=(None, 773), chunks=True)
    training_set = np.zeros((0, 773))
    files = os.listdir(dir)
    random.shuffle(files)
    for file in files:
        if file.endswith('.hdf5'):
            f = h5py.File(os.path.join(dir, file), "r")
            for item in f.keys():
                # print(item,type(f[item].value),len(f[item].value))0
                if len(training_set) < 50000:
                    training_set = np.append(training_set,
                                             convert_List_68_773(f[item].value, 50000 - len(training_set)), axis=0)
                    print("aiciW", training_set.shape, len(X_won))

                else:
                    line = len(X_won)
                    X_won.resize(size=line + len(training_set), axis=0)
                    for x in training_set:
                        X_won[line] = x
                    break
            if len(X_won) >= 50000:
                break
    g1.close()


def getValidation_data():
    val_data = np.zeros((0, 773))
    f1 = "D:\licenta\\ChessAi\\mnt\\games\\whiteLose.hdf5"
    f2 = "D:\licenta\\ChessAi\\mnt\\games\\whiteWon.hdf5"
    f1 = h5py.File(f1, "r")
    f2 = h5py.File(f2, "r")
    for item in f1.keys():
        val_data = np.append(val_data, convert_List_68_773(f1[item].value, limit=10000), axis=0)
    for item in f2.keys():
        val_data = np.append(val_data, convert_List_68_773(f2[item].value, limit=10000), axis=0)
    return np.array(val_data)


def getXWon1():
    # getCurentXWon()
    print("ok")
    f = h5py.File("D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_WonData2_773.hdf5", "r")
    for key in f.keys():
        if key == "x":
            return f[key].value


def getXLost1():
    # getCurentXLost()
    print("ok")

    f = h5py.File("D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_LostData2_773.hdf5", "r")
    for key in f.keys():
        if key == "x":
            return f[key].value


def getXWon():
    getCurentXWon()
    print("ok")
    f = h5py.File("D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_WonData2_773.hdf5", "r")
    for key in f.keys():
        if key == "x":
            return f[key].value


def getXLost():
    getCurentXLost()
    print("ok")

    f = h5py.File("D:\\licenta\\ChessAi\\mnt\\games\\TrainingX_LostData2_773.hdf5", "r")
    for key in f.keys():
        if key == "x":
            return f[key].value


def get_training_data():
    x_won = getXWon()
    x_lose = getXLost()
    print(x_lose.shape)
    print(x_won.shape)
    return np.append(x_won, x_lose, axis=0)


class SAEC:
    def __init__(self):
        self.x_train = get_training_data()
        self.encoder = self.train()

    def train(self):
        # layer wise training of the encoder model
        x_train = np.zeros((200000, 773), dtype=float)
        input = Input(shape=(773,))
        nrNeurons_last_layer = 773

        # set the traning datas
        # x_train = self.x_train.reshape((len(self.x_train), np.prod(self.x_train.shape[1:])))
        # x_test = self.x_test.reshape((len(self.x_test), np.prod(self.x_test.shape[1:])))

        encoded = Dense(600, activation='relu', use_bias=True)(input)
        decoded = Dense(nrNeurons_last_layer, activation='relu', activity_regularizer=regularizers.l2(10e-5))(encoded)
        last_autoencoder = Model(input, decoded)

        # last_autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
        # train the first layer of the encoder
        i = 1
        acc = 0.0
        n = len(x_train)
        while acc < 0.96 or i < 40:
            for j in range(200000):
                k = random.randint(0, n - 1)
                x_train[j] = deepcopy(self.x_train[k])
            last_autoencoder.compile(
                optimizer=optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False),
                loss='mean_squared_error',
                metrics=['mae', 'acc'])

            last_autoencoder.fit(x_train, x_train,
                                 epochs=1,
                                 batch_size=256,
                                 shuffle=True)
            _, _, acc = last_autoencoder.evaluate(x_train, x_train, verbose=0)
            print(i, 600, acc)
            if i % 10 == 0:
                self.x_train = get_training_data()
            i += 1
        list_of_units = [600, 400, 200, 100]

        last_encoder = Model(input, encoded)

        for i, nr_neurons in enumerate(list_of_units):
            if i != 0:
                # we must skip the 1st training becouse we train the layer above
                # at eache iteration we must tace the first i layers form last_autoencoder wich ar diferent form the input
                hidden_layer = Dense(list_of_units[0], activation='relu', use_bias=True,
                                     activity_regularizer=regularizers.l2(10e-5))(input)
                j = 2
                while j <= i:
                    hidden_layer = Dense(list_of_units[j - 1], activation='relu', use_bias=True,
                                         activity_regularizer=regularizers.l2(10e-5))(hidden_layer)
                    j += 1

                # we must add the curent layer that we want to train
                hidden_layer = Dense(nr_neurons, activation='relu', use_bias=True)(hidden_layer)

                out_put = Dense(list_of_units[i - 1], activation='relu', use_bias=True)(hidden_layer)

                encoder = Model(input, hidden_layer)
                autoencoder = Model(input, out_put)
                j = 1
                while j <= i:  # without layers[0] = Input
                    autoencoder.layers[j].set_weights(last_autoencoder.layers[j].get_weights())
                    j += 1

                y_train = last_encoder.predict(x_train)

                q = 1
                acc = 0.0
                while acc < 0.9 or q < 40:
                    for j in range(200000):
                        k = random.randint(0, n - 1)
                        x_train[j] = deepcopy(self.x_train[k])
                    random.shuffle(self.x_train)
                    autoencoder.compile(
                        optimizer=optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False),
                        loss='mean_squared_error',
                        metrics=['mae', 'acc'])

                    autoencoder.fit(x_train, y_train,
                                    epochs=1,
                                    batch_size=256,
                                    shuffle=True)

                    _, _, acc = autoencoder.evaluate(x_train, y_train, verbose=0)
                    print(q, nr_neurons, acc)
                    if q % 10 == 0:
                        self.x_train = get_training_data()
                        if nr_neurons == 100:
                            autoencoder.save("pretrain_NN_E_ADAM12.h5")
                            autoencoder.save_weights("pretrain_NN_E_ADAM12_weights.h5")
                    q += 1

                last_autoencoder = autoencoder
                last_encoder = encoder

        return last_encoder

    def save(self):
        modelToSave = self.encoder.to_yaml()
        with open("encoder/Encoder_Nadam_model.yaml", "w") as yaml_file:
            yaml_file.write(modelToSave)
        self.encoder.save_weights("encoder/Encoder_Nadam_weights.h5")


    def load(self):
        self.encoder = loadModel("encoder/Encoder_Nadam_model.yaml")
        self.encoder.load_weights("encoder/Encoder_Nadam_weights.h5")


if __name__ == "__main__":
    saec = SAEC()
    saec.save()
    s = SAEC()
    s.save()
