from keras.layers import Input, Dense
from keras.models import Model, load_model
import h5py
import numpy as np
import pickle as pk


def convert_To_773(vector_68):
    array_773 = np.zeros(shape=(773,))
    array_773[-4:] = vector_68[-4:]
    for i in range(64):
        if vector_68[i] == 1:
            array_773[i] = 1
        elif vector_68[i] == 2:
            array_773[64 + i] = 1
        elif vector_68[i] == 3:
            array_773[64 * 2 + i] = 1
        elif vector_68[i] == 4:
            array_773[64 * 3 + i] = 1
        elif vector_68[i] == 5:
            array_773[64 * 4 + i] = 1
        elif vector_68[i] == 6:
            array_773[64 * 5 + i] = 1
        elif vector_68[i] == 7:
            array_773[64 * 6 + i] = 1
        elif vector_68[i] == 8:
            array_773[64 * 7 + i] = 1
        elif vector_68[i] == 9:
            array_773[64 * 8 + i] = 1
        elif vector_68[i] == 10:
            array_773[64 * 9 + i] = 1
        elif vector_68[i] == 11:
            array_773[64 * 10 + i] = 1
        elif vector_68[i] == 12:
            array_773[64 * 11 + i] = 1
    return array_773


def convert_List_68_773(list):
    list_modificata = []
    for item in list:
        if len(item) ==68:
            list_modificata.append(convert_To_773(item))
    return np.array(list_modificata)


def getCurentXLost():
    file = "D:\\licenta\\ChessAi\\mnt\\games\\whiteLose.hdf5"
    f = h5py.File(file, "r")
    for item in f.keys():
        # print(item,type(f[item].value))
        return convert_List_68_773(f[item].value)


def getCurentXWon():
    file = "D:\\licenta\\ChessAi\\mnt\\games\\whiteWon.hdf5"
    f = h5py.File(file, "r")
    for item in f.keys():
        return convert_List_68_773(np.array(f[item].value))


class SAEC:
    def __init__(self, train):
        self.x_train = getCurentXLost()
        self.x_test = getCurentXWon()
        self.x_train = self.x_train.astype('float32')
        self.x_test = self.x_test.astype('float32')
        if (train):
            # this is our input placeholder
            input_img = Input(shape=(773,))
            # "encoded" is the encoded representation of the input
            encoded = Dense(600, activation='relu')(input_img)
            encoded = Dense(400, activation='relu')(encoded)
            encoded = Dense(200, activation='relu')(encoded)
            encoded = Dense(100, activation='relu')(encoded)

            decoded = Dense(200, activation='relu')(encoded)
            decoded = Dense(400, activation='relu')(decoded)
            decoded = Dense(600, activation='relu')(decoded)
            decoded = Dense(773, activation='relu')(decoded)

            # this model maps an input to its reconstruction
            self.autoencoder = Model(input_img, decoded)

            self.autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
            x_train = self.x_train.reshape((len(self.x_train), np.prod(self.x_train.shape[1:])))
            x_test = self.x_test.reshape((len(self.x_test), np.prod(self.x_test.shape[1:])))
            print("ok")
            self.autoencoder.fit(x_train, x_train,
                                 epochs=1,
                                 batch_size=256,
                                 shuffle=True,
                                 validation_data=(x_test, x_test))

            self.encoder = Model(input_img, encoded)
            encoded_input = Input(shape=(100,))
            decoder_layer = self.autoencoder.layers[-4](encoded_input)
            decoder_layer = self.autoencoder.layers[-3](decoder_layer)
            decoder_layer = self.autoencoder.layers[-2](decoder_layer)
            decoder_layer = self.autoencoder.layers[-1](decoder_layer)
            self.decoder = Model(encoded_input, decoder_layer)



    def print_outline(self):
        encoded_imgs = self.encoder.predict(self.x_test)
        decoded_imgs = self.decoder.predict(encoded_imgs)
        # decoded_imgs2 = self.autoencoder.predict(self.x_test)
        # print(decoded_imgs[0] == decoded_imgs2[0])

        for j in range(len(self.x_train)):
            nr_out = 0
            for i, nr in enumerate(self.x_train[j]):
                if nr == 1:
                    if (decoded_imgs[j][i] < 0.5):
                        nr_out += 1
                else:
                    if (decoded_imgs[j][i] > 0.5):
                        nr_out += 1
            print("Iteratia ", j, " are eroarea :", nr_out)
            break

    def save(self):
        self.encoder.save("pretrain_NN_E.h5")
        self.decoder.save("pretrain_NN_D.h5")
        self.autoencoder.save(("pretrain_NN_A.h5"))
        file = open("config.pkl", "wb")
        pk.dump({"optimizer": 'adadelta', "loss": 'binary_crossentropy'}, file)

    def load(self):
        self.encoder = load_model("pretrain_NN_E.h5")
        self.decoder = load_model("pretrain_NN_D.h5")
        self.autoencoder = load_model(("pretrain_NN_A.h5"))
        file = open("config.pkl", "rb")
        dict = pk.load(file)
        self.autoencoder.compile(optimizer=dict["optimizer"], loss=dict["loss"])

    def getcfg(self):
        print(self.encoder.layers[1].get_config())
        for i, layer in enumerate(self.encoder.layers):
            try:
                weights = layer.get_weights()
            except:
                continue
            print(i, type(weights))
            for i in range(len(weights)):
                print(type(weights[i]), len(weights[i]))
            prod = 0


if __name__ == "__main__":
    saec = SAEC(True)
    saec.print_outline()
    saec.getcfg()
