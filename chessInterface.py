from time import sleep
import numpy as np
import chess
from keras.models import Model, load_model
from chess_parse import bb2array
from preTrain import convert_To_773
from pygame import *
import pygame

WIN_WIDTH = 32*8
WIN_HEIGHT = 32*8
HALF_WIDTH = int(WIN_WIDTH / 2)
HALF_HEIGHT = int(WIN_HEIGHT / 2)
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)
DEPTH = 32
FLAGS = 0
CAMERA_SLACK = 30

background = pygame.image.load("D:\\licenta\\ChessAi\\mnt\\resurse\\chessbg.png")
spritesheet = pygame.image.load("D:\\licenta\\ChessAi\\mnt\\resurse\\chesspieces.png")
background = pygame.transform.scale(background, (32 * 8, 32 * 8))

character = Surface((39, 42), pygame.SRCALPHA)
character.blit(spritesheet, (-3, 0))
character = pygame.transform.scale(character, (39 * 3, 42 * 3))
white_king = character

character = Surface((39, 42), pygame.SRCALPHA)
character.blit(spritesheet, (-3, -45))
character = pygame.transform.scale(character, (39 * 3, 42 * 3))
black_king = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-48, -49))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
black_queen = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-48, -3))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
white_queen = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-93, -3))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
white_bsp = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-138, -3))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
white_knight = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-182, -3))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
white_rock = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-183, -49))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
black_rock = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-138, -49))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
black_knight = character

character = Surface((39, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-93, -49))
character = pygame.transform.scale(character, (39 * 3, 38 * 3))
black_bsp = character

character = Surface((40, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-226, -3))
character = pygame.transform.scale(character, (40 * 3, 38 * 3))
white_pawn = character

character = Surface((40, 38), pygame.SRCALPHA)
character.blit(spritesheet, (-226, -48))
character = pygame.transform.scale(character, (40 * 3, 38 * 3))
black_pawn = character


class Game:
    def __init__(self):
        self.board = chess.Board()
        self.comparer = load_model('networks\\DeepChess_NN_D.h5')

        # graphics
        pygame.init()
        self.screen = pygame.display.set_mode(DISPLAY, FLAGS, DEPTH)
        pygame.display.set_caption(str("DeepChess"))
        self.timer = pygame.time.Clock()

        self.bg = Surface((WIN_WIDTH, WIN_HEIGHT)).convert()
        self.bg.convert()
        self.bg.fill(Color("#00455b"))

        self.camera = Camera(complex_camera, 32 * 8, 32 * 8)

        self.screen.blit(background,(0,0))

        pygame.display.update()

    def display_Board(self):

        while not self.board.is_game_over():
            print()
            #print(self.board)
            self.display_board(self.board)
            #sleep(1)
            moves = self.board.legal_moves
            # mouve_max_result = moves[0]
            g_max = self.board.copy()
            for move in self.board.legal_moves:
                g_max.push(move)
                break
            for move in self.board.legal_moves:
                g1 = self.board.copy()
                g1.push(move)
                i1 = convert_To_773(bb2array(g_max, False))
                i2 = convert_To_773(bb2array(g1, False))
                x_pred1 = np.zeros(shape=(1, 773),
                                   dtype="float32")
                x_pred2 = np.zeros(shape=(1, 773),
                                   dtype="float32")
                x_pred1[0] = i1
                x_pred2[0] = i2
                # print(i1.shape, i2.shape)
                best = np.argmax(self.comparer.predict(x=[x_pred1, x_pred2]))
                if best == 1:
                    g_max = g1

            self.board = g_max
        print()
        print(self.board, self.board.result(), self.board.turn)

    def display_board(self, board):
        dict = board.piece_map()
        self.screen.blit(background, (0, 0))
        for key in dict:
            #print(dict[key].piece_type)
            col = int(key % 8)
            row = int(key / 8)
            # x[row * 8 + col] = int(dict[key].color) * 7 + dict[key].piece_type
            if dict[key].piece_type == 1:
                p = Pawn(col * 32, row * 32, color=dict[key].color)
                self.screen.blit(p.image, self.camera.apply(p))
            elif dict[key].piece_type == 2:
                p = Knight(col * 32, row * 32, color=dict[key].color)
                self.screen.blit(p.image, self.camera.apply(p))
            elif dict[key].piece_type == 3:
                p = Bsp(col * 32, row * 32, color=dict[key].color)
                self.screen.blit(p.image, self.camera.apply(p))
            elif dict[key].piece_type == 4:
                p = Rook(col * 32, row * 32, color=dict[key].color)
                self.screen.blit(p.image, self.camera.apply(p))
            elif dict[key].piece_type == 5:
                p = Queen(col * 32, row * 32, color=dict[key].color)
                self.screen.blit(p.image, self.camera.apply(p))
            elif dict[key].piece_type == 6:
                p = King(col * 32, row * 32, color=dict[key].color)
                self.screen.blit(p.image, self.camera.apply(p))
        pygame.display.update()


class Camera(object):
    def __init__(self, camera_func, width, height):
        self.camera_func = camera_func
        self.state = Rect(0, 0, width, height)

    def apply(self, target):
        return target.rect.move(self.state.topleft)

    def update(self, target):
        self.state = self.camera_func(self.state, target.rect)


def complex_camera(camera, target_rect):
    l, t, _, _ = target_rect
    _, _, w, h = camera
    l, t, _, _ = -l + HALF_WIDTH, -t + HALF_HEIGHT, w, h

    l = min(0, l)  # stop scrolling at the left edge
    l = max(-(camera.width - WIN_WIDTH), l)  # stop scrolling at the right edge
    t = max(-(camera.height - WIN_HEIGHT), t)  # stop scrolling at the bottom
    t = min(0, t)  # stop scrolling at the top
    return Rect(l, t, w, h)


class Piece(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)


class King(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_king
        else:
            self.image = white_king
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Queen(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_queen
        else:
            self.image = white_queen
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Bsp(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_bsp
        else:
            self.image = white_bsp
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Rook(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color :
            self.image = black_rock
        else:
            self.image = white_rock
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Knight(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color :
            self.image = black_knight
        else:
            self.image = white_knight
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


class Pawn(Piece):
    def __init__(self, x, y, color):
        Piece.__init__(self)
        if not color:
            self.image = black_pawn
        else:
            self.image = white_pawn
        self.image = pygame.transform.scale(self.image, (16 * 2, 16 * 2))
        self.rect = Rect(x, y, 16 * 2, 16 * 2)

    def getCoord(self):
        return int(self.rect.left / 32), int(self.rect.top / 32)

    def update(self):
        pass


if __name__ == "__main__":
    # init_Display()
    game = Game()
    game.display_Board()
