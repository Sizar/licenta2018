package com.example.manol.chessgame.player.ai;

import android.content.Context;

import com.example.manol.chessgame.Alliance;
import com.example.manol.chessgame.board.Board;
import com.example.manol.chessgame.board.Move;
import com.example.manol.chessgame.pieces.Piece;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

public class LevinNNEvaluator implements BoardEvaluatorNN {

    private static final String MODEL_FILE = "file:///android_asset/deployedVersion.pb";
    private static final String INPUT_NAME1 = "input_1";
    private static final String INPUT_NAME2 = "input_2";
    private static final long[] INPUT_SHAPE = new long[2];
    private static final String INPUT_NAME3 = "dropout_1/keras_learning_phase/input";
    private static final String OUTPUT_NAME = "output_node0";

    private TensorFlowInferenceInterface tensorflow;
    public LevinNNEvaluator(Context myContext){
        tensorflow = new TensorFlowInferenceInterface(myContext.getAssets(), MODEL_FILE);
        INPUT_SHAPE[0] = 1;
        INPUT_SHAPE[1] = 773;
    }
    @Override
    public float evaluate(Board initialBoard , Board finalBoard) {
        float[] output = new float[1];
        float[] input1 = convertForPredict(initialBoard);
        float[] input2 = convertForPredict(finalBoard);
        tensorflow.feed(INPUT_NAME1, input1, INPUT_SHAPE); // INPUT_SHAPE is an long[] of expected shape, input is a float[] with the input data
        tensorflow.feed(INPUT_NAME2,input2,INPUT_SHAPE);
        // running inference for given input and reading output

        String[] outputNodes = {OUTPUT_NAME};
        tensorflow.run(outputNodes);
        tensorflow.fetch(OUTPUT_NAME, output); // output is a preallocated float[] in the size of the expected output vector


        return output[0];
    }

    private float[] convertForPredict(final Board board) {
        float[] array = new float[773];
        for (int i = 0; i < 773; ++i) {
            array[i] = (float) 0;
        }

        for (final Piece piece : board.getBlackPieces()) {
            final int position = 63 - piece.getPiecePosition();
            if (piece.getPiceType() == Piece.PieceType.PAWN) {
                array[position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.KINIGHT) {
                array[64 + position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.BISHOP) {
                array[2 * 64 + position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.ROOK) {
                array[3 * 64 + position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.KING) {
                array[4 * 64 + position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.QUEEN) {
                array[5 * 64 + position] = (float) 1;
            }
        }

        for (final Piece piece : board.getWhitePieces()) {
            final int position = 63 - piece.getPiecePosition();
            if (piece.getPiceType() == Piece.PieceType.PAWN) {
                array[6 * 64 + position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.KINIGHT) {
                array[7 * 64 + position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.BISHOP) {
                array[8 * 64 + position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.ROOK) {
                boolean isWhite = piece.getPieceAllience() == Alliance.WHITE;
                array[9 * 64 + position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.KING) {
                array[10 * 64 + position] = (float) 1;
            }
            if (piece.getPiceType() == Piece.PieceType.QUEEN) {
                array[11 * 64 + position] = (float) 1;
            }
        }
        if (board.currentPlayer().getAlliance() == Alliance.WHITE)
            array[768] = 1;
        for (Move move : board.whitePlayer().calculateKingCastles(board.whitePlayer().getLegalMoves(), board.blackPlayer().getLegalMoves())) {
            if (move instanceof Move.KingSideCastleMove)
                array[769] = (float) 1;
            if (move instanceof Move.QueenSideCastleMove)
                array[770] = (float) 1;
        }
        for (Move move : board.blackPlayer().calculateKingCastles(board.blackPlayer().getLegalMoves(), board.whitePlayer().getLegalMoves())) {
            if (move instanceof Move.KingSideCastleMove)
                array[771] = (float) 1;
            if (move instanceof Move.QueenSideCastleMove)
                array[772] = (float) 1;
        }

        return array;
    }
    public void deconvertFromPredict(final Board board) {
        int[] arrayDeconverted = new int[64];
        for (int i = 0; i < 64; ++i) {
            arrayDeconverted[i] = 0;
        }
        float[] array = convertForPredict(board);
        for (int i = 0; i < 768; ++i) {
            if (array[i] == (float) 1) {
                int typePiece = (int) i / 64 + 1;
                arrayDeconverted[i % 64] = typePiece;
            }
        }
        for (int i = 0; i < 8; ++i) {
            int nr = i * 8;
            System.out.println("" + i + ": " + arrayDeconverted[nr] + ' ' + arrayDeconverted[nr + 1] + ' ' + arrayDeconverted[nr + 2] + ' '
                    + arrayDeconverted[nr + 3] + ' ' + arrayDeconverted[nr + 4] + ' ' + arrayDeconverted[nr + 5] + ' ' +
                    arrayDeconverted[nr + 6] + ' ' + arrayDeconverted[nr + 7] + ' ');
        }
        System.out.println("" + array[768] + ' ' + array[769] + ' ' + array[770] + ' ' + array[771] + ' ' + array[772]);
    }
}
