package com.example.manol.chessgame.player.ai;

import com.example.manol.chessgame.Alliance;
import com.example.manol.chessgame.board.Board;
import com.example.manol.chessgame.board.Move;
import com.example.manol.chessgame.pieces.Piece;
import com.example.manol.chessgame.player.MoveTransition;

import android.content.Context;


public class DeepChess implements MoveStrategy {
    private final int depth;
    private BoardEvaluatorNN boardEvalulator;

    public DeepChess(final int depth, Context myContext) {
        this.depth = depth;
        boardEvalulator = new LevinNNEvaluator(myContext);
    }


    @Override
    public Move execute(Board board) {
        final long startTime = System.currentTimeMillis();

        Move bestMove = null;

        float hiestSeenValue = Float.MIN_VALUE;
        float loestSeenValue = Float.MAX_VALUE;
        float currentValue;

        System.out.println(board.currentPlayer() + " THINCKING WITH DEPTH: " + depth);
        int numMoves = board.currentPlayer().getLegalMoves().size();

        for (final Move move : board.currentPlayer().getLegalMoves()) {
            final MoveTransition moveTransition = board.currentPlayer().makeMove(move);
            if (moveTransition.getMoveStatus().isDone()) {
                currentValue = board.currentPlayer().getAlliance().isWhite() ?
                        min(board, moveTransition.getTransitionBoard(), depth - 1,Float.MIN_VALUE,Float.MAX_VALUE) :
                        max(board, moveTransition.getTransitionBoard(), depth - 1,Float.MIN_VALUE,Float.MAX_VALUE);

                if (board.currentPlayer().getAlliance().isWhite() && currentValue > hiestSeenValue) {
                    hiestSeenValue = currentValue;
                    bestMove = move;
                } else if (board.currentPlayer().getAlliance().isBlack() && currentValue < loestSeenValue) {
                    loestSeenValue = currentValue;
                    bestMove = move;
                }
            }
        }
        final long executionTime = System.currentTimeMillis() - startTime;
        return bestMove;
    }


    private float min(final Board initialBoard, final Board finalBoard, final int depth, float alpha, float beta) {
        if (isEndGameScenario(finalBoard)) {
            return 1;
        }
        if (depth == 0) {
            return this.boardEvalulator.evaluate(initialBoard, finalBoard);
        }
        float lowestSeenValue = Float.MAX_VALUE;
        for (final Move move : finalBoard.currentPlayer().getLegalMoves()) {
            final MoveTransition moveTransition = finalBoard.currentPlayer().makeMove(move);
            if (moveTransition.getMoveStatus().isDone()) {
                final float currentValue = max(finalBoard, moveTransition.getTransitionBoard(), depth - 1,alpha,beta);
                if (currentValue < lowestSeenValue) {
                    lowestSeenValue = currentValue;
                }
                if(currentValue < beta){
                    beta = currentValue;
                }
                if(alpha > currentValue){
                    break;
                }
            }
        }
        return lowestSeenValue;
    }


    private float max(final Board initialBoard, final Board finalBoard, final int depth, float alpha, float beta) {
        if (isEndGameScenario(finalBoard)) {
            return -1;
        }
        if (depth == 0) {
            return (-1)*this.boardEvalulator.evaluate(initialBoard, finalBoard);
        }
        float hiestSeenValue = Float.MIN_VALUE;
        for (final Move move : finalBoard.currentPlayer().getLegalMoves()) {
            final MoveTransition moveTransition = finalBoard.currentPlayer().makeMove(move);
            if (moveTransition.getMoveStatus().isDone()) {
                final float currentValue = min(finalBoard, moveTransition.getTransitionBoard(), depth - 1,alpha,beta);
                if (currentValue > hiestSeenValue) {
                    hiestSeenValue = currentValue;
                }
                if(currentValue > alpha){
                    alpha = currentValue;
                }
                if(currentValue > beta)
                    break;
            }
        }
        return hiestSeenValue;
    }

    private static boolean isEndGameScenario(final Board board) {
        return board.currentPlayer().isInCheckMate() ||
                board.currentPlayer().isInStaleMate();
    }

}
