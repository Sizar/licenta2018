package com.example.manol.chessgame.player.ai;

import com.example.manol.chessgame.board.Board;

public interface BoardEvaluatorNN {
    float evaluate(Board initialBoard , Board finalBoard);
}
