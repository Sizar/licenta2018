package com.example.manol.chessgame;

import com.example.manol.chessgame.board.Board;
import com.example.manol.chessgame.board.BoardUtils;
import com.example.manol.chessgame.board.Move;
import com.example.manol.chessgame.pieces.King;
import com.example.manol.chessgame.pieces.Pawn;
import com.example.manol.chessgame.pieces.Piece;
import com.example.manol.chessgame.player.MoveTransition;
import com.example.manol.chessgame.player.ai.BoardEvalulator;
import com.example.manol.chessgame.player.ai.DeepChess;
import com.example.manol.chessgame.player.ai.StandardBoardEvaluator;
import com.google.common.collect.Iterables;

import org.junit.Test;

import static com.example.manol.chessgame.board.Move.MoveFactory.*;
import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void initialBoard() {

        final Board board = Board.createStandardBoard();
        assertEquals(board.currentPlayer().getLegalMoves().size(), 20);
        assertEquals(board.currentPlayer().getOpponent().getLegalMoves().size(), 20);
        assertFalse(board.currentPlayer().isInCheck());
        assertFalse(board.currentPlayer().isInCheckMate());
        assertFalse(board.currentPlayer().isCastled());
        //assertTrue(board.currentPlayer().isKingSideCastleCapable());
        //assertTrue(board.currentPlayer().isQueenSideCastleCapable());
        assertEquals(board.currentPlayer(), board.whitePlayer());
        assertEquals(board.currentPlayer().getOpponent(), board.blackPlayer());
        assertFalse(board.currentPlayer().getOpponent().isInCheck());
        assertFalse(board.currentPlayer().getOpponent().isInCheckMate());
        assertFalse(board.currentPlayer().getOpponent().isCastled());
       // assertTrue(board.currentPlayer().getOpponent().isKingSideCastleCapable());
       // assertTrue(board.currentPlayer().getOpponent().isQueenSideCastleCapable());
        //assertTrue(board.whitePlayer().toString().equals("White"));
        //assertTrue(board.blackPlayer().toString().equals("Black"));

        final Iterable<Piece> allPieces = board.getAllPieces();
        final Iterable<Move> allMoves = Iterables.concat(board.whitePlayer().getLegalMoves(), board.blackPlayer().getLegalMoves());
        for(final Move move : allMoves) {
            assertFalse(move.isAttack());
            assertFalse(move.isCastlingMove());
           // assertEquals(MoveUtils.exchangeScore(move), 1);
        }

        assertEquals(Iterables.size(allMoves), 40);
        assertEquals(Iterables.size(allPieces), 32);
        assertFalse(BoardUtils.isEndGame(board));
        //assertFalse(BoardUtils.isThreatenedBoardImmediate(board));
        //assertEquals(StandardBoardEvaluator.get().evaluate(board, 0), 0);
        assertEquals(board.getTile(35).getPiece(), null);
    }

    @Test
    public void testPlainKingMove() {

        final Board.Builder builder = new Board.Builder();
        // Black Layout
        builder.setPiece(new King( 4,Alliance.BLACK));
        builder.setPiece(new Pawn(12,Alliance.BLACK ));
        // White Layout
        builder.setPiece(new Pawn(52,Alliance.WHITE));
        builder.setPiece(new King(60,Alliance.WHITE));
        builder.setMoveMaker(Alliance.WHITE);
        // Set the current player
        final Board board = builder.build();
        System.out.println(board);

        assertEquals(board.whitePlayer().getLegalMoves().size(), 6);
        assertEquals(board.blackPlayer().getLegalMoves().size(), 6);
        assertFalse(board.currentPlayer().isInCheck());
        assertFalse(board.currentPlayer().isInCheckMate());
        assertFalse(board.currentPlayer().getOpponent().isInCheck());
        assertFalse(board.currentPlayer().getOpponent().isInCheckMate());
        assertEquals(board.currentPlayer(), board.whitePlayer());
        assertEquals(board.currentPlayer().getOpponent(), board.blackPlayer());
//        BoardEvalulator evaluator = StandardBoardEvaluator.get();
//        System.out.println(evaluator.evaluate(board, 0));
//        assertEquals(StandardBoardEvaluator.get().evaluate(board, 0), 0);
//
//        final Move move = createMove(board, BoardUtils.INSTANCE.getCoordinateAtPosition("e1"),
//                BoardUtils.INSTANCE.getCoordinateAtPosition("f1"));
//
//        final MoveTransition moveTransition = board.currentPlayer()
//                .makeMove(move);
//
//        assertEquals(moveTransition.getTransitionMove(), move);
//        assertEquals(moveTransition.getFromBoard(), board);
//        assertEquals(moveTransition.getToBoard().currentPlayer(), moveTransition.getToBoard().blackPlayer());
//
//        assertTrue(moveTransition.getMoveStatus().isDone());
//        assertEquals(moveTransition.getToBoard().whitePlayer().getPlayerKing().getPiecePosition(), 61);
//        System.out.println(moveTransition.getToBoard());

    }

    @Test
    public void testDeepChess(){
        DeepChess dc = new DeepChess();
        Board board = Board.createStandardBoard();
        dc.deconvertFromPredict(board);
    }
}