import keras
from keras.models import Model, load_model
from keras.layers.core import Dense, Dropout, Activation
from keras.layers import Input, Layer
from preTrain import getCurentXWon, getCurentXLost
import numpy as np


class DeepChess:
    def __init__(self):
        # get the model that encode the states
        encoder = load_model("pretrain_NN_E.h5")

        # createing the firsts layers
        input1 = Input(shape=(773,))  # the input
        encoded1 = Dense(600, activation='relu')(input1)
        encoded1 = Dense(400, activation='relu')(encoded1)
        encoded1 = Dense(200, activation='relu')(encoded1)
        encoded1 = Dense(100, activation='relu')(encoded1)

        input2 = Input(shape=(773,))  # the input
        encoded2 = Dense(600, activation='relu')(input2)
        encoded2 = Dense(400, activation='relu')(encoded2)
        encoded2 = Dense(200, activation='relu')(encoded2)
        encoded2 = Dense(100, activation='relu')(encoded2)

        # set the weights
        x = keras.layers.concatenate([encoded1, encoded2])

        # create the top layers for the actualy comparing the to states
        hidden_layer = Dense(400, activation='relu', use_bias=True)(x)
        hidden_layer = Dense(200, activation='relu', use_bias=True)(hidden_layer)
        hidden_layer = Dense(100, activation='relu', use_bias=True)(hidden_layer)
        hidden_layer = Dense(2, activation='softmax')(hidden_layer)
        self.deepChees = Model(inputs=[input1, input2], outputs=hidden_layer)

        # now lets fix the layers form the start of our network
        j = 1
        for i in range(2, 10, 2):
            # print(i)
            self.deepChees.layers[i].set_weights(encoder.layers[j].get_weights())
            self.deepChees.layers[i + 1].set_weights(encoder.layers[j].get_weights())
            j += 1
        # configure the optimizer and loss, here I used adadelta and binart_crssentropy, feel free to try to explore
        self.deepChees.compile(optimizer='adadelta', loss='binary_crossentropy')

        print("################# Init done #################")

    def train(self):
        print("################# Starting the training #################")
        # geting the encodings
        x_Won = getCurentXWon()
        x_Lose = getCurentXLost()

        # it's better to compute 1 single time
        n_Won = len(x_Won)
        n_Lose = len(x_Lose)

        # print(np.concatenate((x_Lose[0], x_Won[0])).shape)

        # train data
        x_train1 = np.zeros(shape=(1000000, 773),
                            dtype="float32")  # 10000000 of inputs ,2 * 100 the size of the encode.predict(...)
        x_train2 = np.zeros(shape=(1000000, 773),
                            dtype="float32")  # 10000000 of inputs ,2 * 100 the size of the encode.predict(...)
        y_train = np.zeros((1000000, 2))  # 1000000 of outputs

        # validating data

        x_validate1 = np.zeros(shape=(1000, 773),
                               dtype="float32")  # 1000000 of inputs ,2 * 100 the size of the encode.predict(...)
        x_validate2 = np.zeros(shape=(1000, 773),
                               dtype="float32")  # 1000000 of inputs ,2 * 100 the size of the encode.predict(...)
        y_validate = np.zeros((1000, 2))  # 1000000 of outputs

        for i in range(1000):
            if np.random.randint(0, 2) == 1:
                indexWon = np.random.randint(0, n_Won)
                indexLose = np.random.randint(0, n_Lose)
                x_validate1[i] = np.copy(x_Lose[indexLose])
                x_validate2[i] = np.copy(x_Won[indexWon])
                y_validate[i][1] = 1
            else:
                indexWon = np.random.randint(0, n_Won)
                indexLose = np.random.randint(0, n_Lose)
                x_validate2[i] = np.copy(x_Won[indexWon])
                x_validate1[i] = np.copy(x_Lose[indexLose])
                y_validate[i][0] = 1

        for j in range(1000):
            # gating the data for training
            for i in range(1000000):
                if np.random.randint(0, 2) == 1:
                    indexWon = np.random.randint(0, n_Won)
                    indexLose = np.random.randint(0, n_Lose)
                    x_train1[i] = np.copy(x_Lose[indexLose])
                    x_train2[i] = np.copy(x_Won[indexWon])
                    y_train[i][1] = 1
                else:
                    indexWon = np.random.randint(0, n_Won)
                    indexLose = np.random.randint(0, n_Lose)
                    x_train1[i] = np.copy(x_Won[indexWon])
                    x_train2[i] = np.copy(x_Lose[indexLose])
                    y_train[i][0] = 1

            # x_train = x_train.reshape((len(x_train), np.prod(x_train.shape[1:])))
            # x_validate = x_validate.reshape((len(x_validate), np.prod(x_validate.shape[1:])))
            # print("ok")

            self.deepChees.fit([x_train1, x_train2], y_train,
                               epochs=1,
                               batch_size=256,
                               shuffle=True,
                               validation_data=([x_validate1, x_validate2], y_validate)
                               )
            self.deepChees.save("networks\\DeepChess_NN_D.h5")
        print("################# Training done! #################")


if __name__ == "__main__":
    dc = DeepChess()
    dc.train()
    # dc.deepChees.predict()
